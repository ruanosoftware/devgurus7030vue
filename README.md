# DevGurus 70/30 - Vue3

### Description
The code for this exercise can be executed locally or online in CodeSandbox

### Execute code locally

#### Clone the code
```Shell
git clone https://ruanosoftware@bitbucket.org/ruanosoftware/devgurus7030vue.git
```

#### Install dependencies
```Shell
npm install
```

#### Run
```Shell
npm run dev
```

> Open url at [localhost:3000](localhost:3000)

### Exercise in CodeSandbox

* [Group 1](https://codesandbox.io/s/devgurus7030group01-c64bj)
* [Group 2](https://codesandbox.io/s/devgurus7030group02-mckic)
* [Group 3](https://codesandbox.io/s/devgurus7030group03-y7rgt)
* [Group 4](https://codesandbox.io/s/devgurus7030group04-2iheh)
* [Group 5](https://codesandbox.io/s/devgurus7030group05-tgx5t)

### Instructions

Run the application and press *instructions* link.
Follow the steps in guide
