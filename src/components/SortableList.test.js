import { mount } from "@vue/test-utils";
import SortableList from "./SortableList.vue";

describe("Sortable List", () => {
  it("items list initially contains 5 elements", () => {
    const wrapper = mount(SortableList);
    expect(wrapper.vm.state.list.length).toBe(5);
  });

  it("5 items are rendered", () => {
    const wrapper = mount(SortableList);
    const todo = wrapper.findAll(".item-base");
    expect(todo.length).toBe(5);
  });

  it("add new element to list", () => {
    const wrapper = mount(SortableList);

    // before adding new element
    expect(wrapper.vm.state.list.length).toBe(5);

    // add new item
    const inputText = wrapper.find("#description");
    inputText.element.value = "New item";
    inputText.trigger("input");

    // after adding new element
    wrapper.find("#addNew").trigger("click");
    expect(wrapper.vm.state.list.length).toBe(6);
  });


  it("delete element from list", () => {
    const wrapper = mount(SortableList);

    // before adding new element
    expect(wrapper.vm.state.list.length).toBe(5);

    // emulate drop event on trashcan
    const trashCan = wrapper.find("#trashcan");
    trashCan.trigger("drop", {
        dataTransfer: {
            getData(){return '0'}
        }
    });

    // after deleting element
    expect(wrapper.vm.state.list.length).toBe(4);
  });
});
